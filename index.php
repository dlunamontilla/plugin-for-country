<?php

/**
 * Plugin Name: Plugin For Country
 * Plugin URI: https://gitlab.com/dlunamontilla/plugin-for-country
 * Description: Este plugin fue hecho para una prueba técnica. 
 * Version: 1.0.0
 * Author: David E Luna M
 * Author URI: https://aprendiendo.netlify.app/
 * License: MIT
 */


add_shortcode("country", function () {
	wp_enqueue_style('build');
	wp_enqueue_script('build');

	$api = plugin_dir_url( __FILE__ );


	$html = "<div id=\"app\" data-url=\"$api" . "api\"></div>";
	return $html;
});


add_action('init', function () {
	wp_register_style('build', plugin_dir_url(__FILE__) . "build/bundle.css");
	wp_register_script('build', plugin_dir_url(__FILE__) . "build/bundle.js");
});

add_filter( 'script_loader_tag', function($tag, $handle, $src) {
	return $handle === "build"
		? "<script src=\"$src\" defer></script>"
		: $tag;
}, 10, 3);