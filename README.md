# Plugin For Country

> **Importatne:** lea todas las instrucciones antes de comenzar.

Es una extensión hecha para WordPress y fue escrita en PHP, JavaScript, Svelte y SASS.

## Clonar mediante SSH

Para clonar el proyecto mediante SSH puede escribir la siguiente línea:

```bash
git clone git@gitlab.com:dlunamontilla/plugin-for-country.git
```

Para el caso anterio, debe tener su cuenta configurada con una llave SSH.

## Clonar mediante HTTP

Para clonar el proyecto mediante HTTP escriba la siguiente línea en la terminal o símbolo del sistema:

```bash
git clone https://gitlab.com/dlunamontilla/plugin-for-country.git
```

Inicialmente, debería ver algo como esto:

![Vista previa sin seleccionar país][1]

Sin embargo, en la medida que seleccione un país, le aparecerá las opciones de departamento o estado, municipio y ciudad:

![Vista previa una vez que haya seleccionado un país][2]

[1]:https://i.ibb.co/rQ3Q8VY/imagen.png "Vista previa incial"
[2]:https://i.ibb.co/KzYsqmb/imagen.png "Vista previa"

<br>

---

## Instalación del plugin en Wordpress

Existen varias formas de instalar el plugin en WordPress. Una es comprimiendo la carpeta `plugin-for-country` y subirla en WordPress. Para ello vaya al módulo **Plugins** y luego presione el botón **Agregar nuevo** y finalmente presione el botón *Subir extensión**. Una vez haya instalado el plugin, solo tienes que activarlo.

La otra forma de instalarlo es copiando la carpeta `plugin-for-country` al directorio de plugins de WordPress. Para ello diríjase a:

```none
wp-content/plugins/
```

Y pegue el proyecto allí.

## Uso del plugin `plugin-for-country`

Para agregar formularios en su página o entrada (post) solo debe insertar un pequeño `shortcode`:

```none
[country]
```

Y donde se encuentre **`[country]`** aparecerá el formulario.


Una actualización reciente.
