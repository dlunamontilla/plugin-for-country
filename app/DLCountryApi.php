<?php
ini_set('display_errors', '1');

class DLCountryApi {
    /**
     * @var string Usuario de la API que se va a llamar
     */
    private $username;

    /**
     * @var string Ruta para obtener una lista de regiones o municipios
     * en función del país que se haya elegido.
     */
    private $region = 'http://api.geonames.org/childrenJSON?lang=es&username=';

    /**
     * @var string Ruta para obtener una ciudad en función de un
     * cuadro de deliminación a partir de una región seleccionada.
     */
    private $cities = 'http://api.geonames.org/citiesJSON?lang=es&username=';

    /**
     * @param string $username Usuario de la API que va a consumir.
     */
    public function __construct(string $username = "dlunamontilla") {
        $this->username = $username . "&lang=es";
        $this->region .= $username . "&lang=es&style=full";
        $this->cities .= $username;
    }

    /**
     * @param string $url Ruta del recurso que se desea obtener.
     * @return string
     * 
     * Obtiene un recurso a partir de una ruta.
     */
    private function getData(string $url): string {
        $url = (string) $url;

        /**
         * @var string | false
         */
        $data = @file_get_contents($url, false);
        return $data;
    }

    /**
     * @param ?string $url Ruta de la API de países.
     * @return array
     */
    public function getCountries(string $url = "http://api.geonames.org/countryInfoJSON?lang=es&username="): array {
        $stringData = $this->getData($url . $this->username);

        $data = (array) json_decode($stringData);
        $countries = [];

        if (is_array($data) && array_key_exists("geonames", $data)) foreach($data["geonames"] as $key => $country) {
            array_push($countries, [
                "code" => $country->fipsCode,
                "geonameId" => $country->geonameId,
                "countryName" => $country->countryName
            ]);
        }
        
        return $countries;
    }


    /**
     * @param ?string $geonameId Es el identificador de una región.
     * @return array | object
     */
    public function getRegions(string $geonameId = '3625428'): array|object {
        $this->region .= '&geonameId=' . $geonameId;
        $region = $this->getData($this->region);
        
        return json_decode($region)
            ? json_decode($region)
            : [];
    }

    /**
     * @param array $coordinates Coordenadas del cuadro delimitador 
     * donde se pretende encontrar ciudades.
     * @return array | object
     */
    public function getCities(array $coordinates): array|object {
        extract($coordinates);
        $this->cities .= "&north=$north&south=$south&east=$east&west=$west";
        $cities = $this->getData($this->cities);
        return json_decode($cities);
    }
}