# Clase DLCountryApi

Consume una *API* dada para listar países en todo el planeta.

**Sintaxis:**

```php
DLCountryApi::__construct(string $endpoint = "http://api.geonames.org/countryInfoJSON?username=", string $username = "dlunamontilla");
```

Donde `$endpoint` es la ruta de la API y `$username` es el nombre del usuario asociada a la API. Ambos parámetros son opcionales y por lo tanto se puede instanciar la clase de la siguiente manera, sin colocar parámetros:

```php
$country = new DLCountryApi;
```

De esta forma, la ruta predeterminada será la siguiente:

```none
http://api.geonames.org/countryInfoJSON?username=dlunamontilla
```

Si desea establecer otra ruta deberá instanciarla así:

```php
$country = new DLCountryApi('http://tu-api.com/api', 'tu-usuario');
```

<br>

---

<small>Documentado por **David E Luna M - Desarrollador Web.**</small>