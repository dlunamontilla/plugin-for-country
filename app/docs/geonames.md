# Uso de la API de Geo Names

Conociendo los parámetros de una petición:

| Parámetro     | ¿Qué es?                                                                                                                                                                                                                                       |
| ------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `geonameId`   | Es el padre de los elementos que se están consultando. Por ejemplo, un país, región y/o municipio tiene un `geonameId`.                                                                                                                        |
| `maxRows`     | Devuelve un número de registros o filas. El valor predeterminado es 200.                                                                                                                                                                       |
| `hierarchy`   | Este parámetro es opcional. Permite utilizar otras jerarquías, además de las jerarquías predeterminadas. Los valores posibles son: `tourism` para regiones turísticas, `geography` para regiones geográficas y `dependency` para dependencias. |
| `lang`        | Con este parámetro se establece el idioma de la API. Por ejemplo, para obtener datos en español debe establecerse así: `&lang=es`.                                                                                                             |
| `citiesJSON?` | Esta ruta debe ir acompañado de forma obligatoria los parámetros que delimitan la zona donde se encuentran las ciudades.                                                                                                                       |

Donde `geonameId` es el nombre del país al que se le quiere listar los _estados/provincias/departamentos_.

Acá tiene un ejemplo de cómo podrías obtener los datos con JavaScript: `&north={north}&south={south}&east={east}&west={west}`

```js

async function getData(url) {
    const response = await fetch(url)
    const data = await response.json();

    const states = [];

    if (Array.isArray(data)) data.forEach(register => {
        const { toponymName } = register;
        states.push({ stateName: toponymName });
    });

    console.log( states );
}

getData('http://api.geonames.org/childrenJSON?geonameId=3625428&username=su-usuario');
```

Se utilizó `toponymName` mediante desestructuración de cada registro en cada iteración para obtener el nombre de cada estado o región de un país determinado.

Dado que **Geo Names** no cuenta con un certificado válido se recomienda obtener los datos directamente desde el _backend_.

## Obtener nombre de estados o provincias

Si desea obtener una lista de estados por país utilice la siguiente ruta:

```none
http://api.geonames.org/childrenJSON?geonameId=3625428&username=su-usuario
```

## Obtener una lista de municipios

Para obtener una lista de municipios pertenecientes a un estado, región, provincias o departamento utilice la siguiente ruta (_endpoint_).

```none
http://api.geonames.org/childrenJSON?geonameId=3640873&username=dlunamontilla
```

Donde `geonameId` tiene un `ID` de un estado perteneciente a un país determinado.

## Obtener una lista de ciudades

Para obtener una lista de ciudades en función de un municipio o estado utilice las rutas que siguen más abajo.

**Por municipios:**

```none
http://api.geonames.org/citiesJSON?north=11.74037&south=11.64267&east=-70.14929&west=-70.24908&lang=es&username=dlunamontilla
```

**Importante:** para encontrar ciudades dentro de un estado o provincia o municipio se deben conocer su cuadro de deliminación. Para ello se debe utilizar el parámetro `&style=full` en la siguiente ruta:

```none
http://api.geonames.org/childrenJSON?geonameId=3640873&username=dlunamontilla&style=full
```

Para lograr obtener los datos que nos interesa:

```json
"bbox": {
    "east": -70.1492922329039,
    "south": 11.642669983899147,
    "north": 11.740366016100854,
    "west": -70.2490757670961,
    "accuracyLevel": 2
}
```

Para utilizarlos en la siguiente ruta:

```none
http://api.geonames.org/citiesJSON?north={north}&south={south}&east={east}&west={west}&lang=es&username=dlunamontilla
```
