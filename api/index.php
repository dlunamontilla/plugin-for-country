<?php
header("content-type: application/json; charset=utf-9");

$include = __DIR__ . "/../app/autoload.php";

if (!file_exists($include)) exit;

include $include;

/**
 * @var \DLCountryApi
 */
$countries = new DLCountryApi;
$request = new DLRequest;

if ($request->get(["countries" => FALSE])) {
    $listCountries = $countries->getCountries();

    if (is_array($listCountries) || is_object($listCountries)) {
        echo json_encode($listCountries);
    }
}

/**
 * Listar áreas administrativas. Puede ser estados, provincias o departamentos de
 * un país determinado.
 * 
 * @var array Parámetros de validación. No se permite que esté vacío.
 */

$region = [
    "id-pais" => TRUE
];

if ($request->get($region)) {
    $regions = $countries->getRegions((string) $request->getValue("id-pais"));
    
    if (is_array($regions) || is_object($regions)) {
        echo json_encode($regions);
    }
}


/**
 * @var array Parámetros para obtener una lista de ciudades a partir de deliminatores
 * en función de una región o municipio.
 */

$city = [
    "north" => TRUE,
    "south" => TRUE,
    "east" => TRUE,
    "west" => TRUE
];

if ($request->get($city)) {
    $cities = $countries->getCities((array) $request->getValues());
    echo json_encode($cities);
}